const db = require("../lib/db")
const bcrypt = require("bcrypt")

const createUsersTable = async () => {
    await db.query("CREATE TABLE IF NOT EXISTS users(id SERIAL PRIMARY KEY NOT NULL, gebruikersnaam VARCHAR(30) NOT NULL, wachtwoord VARCHAR(255) NOT NULL);");

    const saltRounds = 10;
    const plainTextPassword1 = "wachtwoord";

    await db.query("SELECT EXISTS(SELECT 1 FROM users WHERE gebruikersnaam = 'harry')", function(err, row) {
        if(err) {
            console.error('Error in DB');
            console.debug(err);
        } else {
            if (row && row.rows.length ) {
                console.log('Case row was found!');
            } else {
                console.log('No case row was found :( !');
                bcrypt
                    .hash(plainTextPassword1, saltRounds)
                    .then(hash => {
                        db.query("INSERT INTO users(gebruikersnaam, wachtwoord) VALUES ('harry', $1);", [hash]);
                    })
                    .catch(err => console.error(err.message));
            }
        }
    });

}

const createPlantenTable = async () => {
    await db.query("CREATE TABLE IF NOT EXISTS planten(id SERIAL PRIMARY KEY NOT NULL, dropped SMALLINT NOT NULL, tijd TIMESTAMP);");
}

const dropUsersTable = async (callback) => {
    await db.query("DROP TABLE IF EXISTS users;");
}

const dropPlantenTable = async () => {
    await db.query("DROP TABLE IF EXISTS planten;");
}

const createAllTables = () => {
    createUsersTable();
    createPlantenTable();
}

const dropAllTables = () => {
    dropUsersTable();
    dropPlantenTable();
}

module.exports = {
    createUsersTable,
    createPlantenTable,
    dropUsersTable,
    dropPlantenTable,
    createAllTables,
    dropAllTables
};

require('make-runnable')
