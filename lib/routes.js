const express = require("express");
const app = express();
const passport = require("passport");
const os = require("os");
const request = require("request");
const connectEnsureLogin = require("connect-ensure-login");
const db = require("../lib/db")
const bcrypt = require("bcrypt")

app.use(express.static("public"));

const LocalStrategy = require("passport-local").Strategy;

module.exports = function (app) {
    // Home screen
    app.get("/", function (req, res, next) {
        if (req.isAuthenticated()) {
            res.render("index", {
                title: "KASanova | Home",
                username: req.user[0].gebruikersnaam,
                messages: {danger: req.flash("danger"), warning: req.flash("warning"), success: req.flash("success")}
            });
        } else {
            req.session.returnTo = req.originalUrl;
            res.redirect("/login");
        }
    });

    // Login
    app.get("/login", function (req, res, next) {
        if (req.isAuthenticated()) {
            res.redirect("/");
        } else {
            res.render("login", {
                title: "KASanova | Login",
                userData: req.user,
                messages: {danger: req.flash("danger"), warning: req.flash("warning"), success: req.flash("success")}
            });
        }

    });

    app.post("/login", passport.authenticate("local", {
        successReturnToOrRedirect: "/",
        failureRedirect: "/login",
        failureFlash: true
    }), function (req, res) {
        if (req.body.remember) {
            req.session.cookie.maxAge = 30 * 24 * 60 * 60 * 1000; // Cookie expires after 30 days
        } else {
            req.session.cookie.expires = false; // Cookie expires at end of session
        }
    });

    // Logout
    app.get("/logout", function (req, res) {
        req.logout();
        req.flash("success", "Uitgelogd!");
        res.redirect("/login");
    });

    // Simulatie
    app.get("/simulatie", function (req, res) {
        if (req.isAuthenticated()) {
            res.render("simulatie", {
                title: "KASanova | Simulatie",
                username: req.user[0].gebruikersnaam,
                messages: {danger: req.flash("danger"), warning: req.flash("warning"), success: req.flash("success")}
            });
        } else {
            req.session.returnTo = req.originalUrl;
            res.redirect("/login");
        }
    });

    app.post("/resetPlanten", function (req, res) {
        db.query("TRUNCATE TABLE planten;");
    });
}

// Login magic
passport.use("local", new LocalStrategy({passReqToCallback: true}, async (req, username, password, done) => {

        loginAttempt();

        async function loginAttempt() {

            try {
                await db.query("BEGIN")
                // const currentAccountsData = await JSON.stringify(
                await db.query("SELECT id, gebruikersnaam, wachtwoord FROM users WHERE gebruikersnaam=$1", [username], async (err, result) => {
                    if (err) {
                        return done(err)
                    }
                    if (result.rows[0] == null) {
                        req.flash("danger", "Incorrecte login!");
                        return done(null, false);
                    } else {
                        function comparePwd(password, hash) {
                            return new Promise(function (resolve, reject)
                            {
                                bcrypt.compare(password, hash, (err, check) => {
                                    if (err) {
                                        console.log("Fout bij het checken van het wachtwoord!");
                                        return done();
                                    } else if (check) {
                                        return done(null, [{
                                            id: result.rows[0].id,
                                            gebruikersnaam: result.rows[0].gebruikersnaam
                                        }]);
                                    } else {
                                        req.flash("danger", "Incorrecte login!");
                                        return done(null, false);
                                    }
                                });
                            });
                        }
                        const res = await comparePwd(password, result.rows[0].wachtwoord);
                    }
                });
                await db.query("COMMIT")
            } catch (e) {
                throw (e);
            }
        }
    }
))

passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (user, done) {
    done(null, user);
});
