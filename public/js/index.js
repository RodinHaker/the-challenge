window.onload = function () {
    const minimize_btn = document.getElementById("minimize"),
        maximize_btn = document.getElementById("maximize"),
        leftnav = document.getElementById("left-nav"),
        topnav = document.getElementById("top-nav"),
        main = document.getElementById("main"),
        dashboard = document.getElementById("dashboard"),
        account = document.getElementById("account"),
        dashboard_act = document.getElementById("dashboard-action"),
        account_act = document.getElementById("account-action"),
        buttons = [dashboard, account],
        actions = [dashboard_act, account_act];

    minimize_btn.addEventListener("click", minimize, false);
    maximize_btn.addEventListener("click", maximize, false);

    function minimize() {
        leftnav.style.left = "-200px";
        topnav.style.left = "0px";
        topnav.style.width = "100%";
        main.style.left = "0px";
        main.style.width = "100%";
        maximize_btn.style.transform = "scale(1)";
    }

    function maximize() {
        leftnav.style.left = "0px";
        topnav.style.left = "200px";
        topnav.style.width = "calc(100% - 200px)";
        main.style.left = "200px";
        main.style.width = "calc(100% - 200px)";
        maximize_btn.style.transform = "scale(0)";
    }

    function show(index){
        for(let i = 0; i < actions.length; i++) {
            if(i !== index) {
                actions[i].style.display = "none";
            }else{
                actions[i].style.display = "block";
            }
        }
    }

    show(0);

    for(let i = 0; i < actions.length; i++) {
        (function() {
            buttons[i].addEventListener("click", function() {show(i); }, false);
        }());
    }

    google.load('visualization', '1.0', {'packages':['corechart']});
    google.setOnLoadCallback(drawChart);

    function incrementPieData(){

    }

    function drawChart(data1, data2, data3, data4) {

        var data1 = google.visualization.arrayToDataTable([
            ['Task', 'Planten per dag'],
            ['Goed',     982],
            ['Slecht',   92]
        ]);

        var data2 = google.visualization.arrayToDataTable([
            ['Task', 'Planten per week'],
            ['Goed',     6246],
            ['Slecht',   823]
        ]);

        var data3 = google.visualization.arrayToDataTable([
            ['Task', 'Planten per maand'],
            ['Goed',     25382],
            ['Slecht',   2742]
        ]);
        var data4 = google.visualization.arrayToDataTable([
            ['Task', 'Planten per half jaar'],
            ['Goed',     151283],
            ['Slecht',   13482]
        ]);


        var options1 = {
            title: 'Planten per dag'
        };

        var options2 = {
            title: 'Planten per week'
        };

        var options3 = {
            title: 'Planten per maand'
        };

        var options4 = {
            title: 'Planten per half jaar'
        };

        var chart1 = new google.visualization.PieChart(document.getElementById('piechart1'));
        var chart2 = new google.visualization.PieChart(document.getElementById('piechart2'));
        var chart3 = new google.visualization.PieChart(document.getElementById('piechart3'));
        var chart4 = new google.visualization.PieChart(document.getElementById('piechart4'));

        chart1.draw(data1, options1);
        chart2.draw(data2, options2);
        chart3.draw(data3, options3);
        chart4.draw(data4, options4);
    }

    window.onresize = drawChart();
}

