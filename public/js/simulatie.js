window.onload = function () {
    const reset_btn = document.getElementById("reset-btn"),
        aan_uit_slider = document.getElementById("aan-uit-slider");

    reset_btn.addEventListener("click", resetPlanten, false);

    function resetPlanten() {
        event.preventDefault();
        $.post('/resetPlanten', function(resp) {
            alert(resp);
        })
    }
}
